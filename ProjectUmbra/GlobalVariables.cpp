#include "stdafx.h"
#include <vector>
#include <SFML/Graphics.hpp>
#include "GlobalVariables.h"
#include "FileDataClasses.h"

int screenSizeX = 1072;
int screenSizeY = 604;

int localPlayerNumber = 0;
int iAmPlayerNumber = 0;

sf::RenderWindow mainWindow(sf::VideoMode(screenSizeX, screenSizeY, 32), "Project UMBRA - Alpha");
sf::View view1(sf::Vector2f((float)screenSizeX / 2, (float)screenSizeY / 2), sf::Vector2f((float)screenSizeX, (float)screenSizeY));;

sf::Time localElapsedTime;											//Time elapsed in main game window
sf::Time timeSinceLastUpdate;										//Time since last update in main game window
const sf::Time timePerFrame = sf::seconds(1.f / 60.f);				//Game logic speed
sf::Clock mainGameClock;											//Clock that counts time elapsed in main game window


bool gameHasFocus;													//Does the game currently have focus? (To avoid essentially key logging)
std::vector<sf::Texture>texturesList();

/*--------- MAP DATA --------------*/
unsigned int mapVersion;
unsigned int numOfTilesets;

unsigned int mapSizeX, mapSizeY;

std::vector < std::string > tileLocationsList;
std::vector < sf::Texture > tileTexturesList;

std::vector < std::vector < tileDeco > > backgroundTilesList;
std::vector < std::vector < tile > > mainTilesList;
std::vector < std::vector < tileDeco > > foregroundTilesList;
std::vector < monster > monstersList;

/*--------- MAP DRAWING ----------------*/
sf::Vector2f viewCenter;
sf::Vector2f viewSize;
int drawMapX1, drawMapX2, drawMapY1, drawMapY2;
float scrollSpeed = (float)2.1;
bool enableScroll = true;

/*------------ GAME DATA ------------------*/
std::vector < sf::Texture > characterTexturesList;
std::vector < sf::Sprite > astarDebugList;
sf::Texture blackTexture;
sf::Texture markerTexture;
//std::vector < player > playersList;