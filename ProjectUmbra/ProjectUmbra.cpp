// ProjectUmbra.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <SFML/Graphics.hpp>

#include "GlobalVariables.h"
#include "FileManager.h"
#include "GameManager.h"
#include "CharacterManager.h"
#include "AStar.h"

void scrollView();

character player;
bool displayPath = true;

int main()
{
	LoadMap("Resources/demo.umbra");
	view1.setCenter((float)screenSizeX / 2, (float)(screenSizeY / 2 + 32));
	mainWindow.setFramerateLimit(60);
	
	sf::Texture playerSprite;
	if (!playerSprite.loadFromFile("Resources/playerOne.png"))
	{
		std::cout << "Error loading player texture!" << std::endl;
	}
	player.sprite.setTexture(playerSprite);
	player.sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	player.sprite.setPosition(64, 64);
	player.speed = (float)1.5;

	if (!markerTexture.loadFromFile("Resources/marker.png"))
	{
		std::cout << "Error loading marker texture!" << std::endl;
	}
	
	sf::Sprite darkness;
	if (!blackTexture.loadFromFile("Resources/darkness.png"))
	{
		std::cout << "Error loading darkness!" << std::endl;
	}
	darkness.setTexture(blackTexture);

	sf::Texture monsterTexture;
	monster mob;
	if (!monsterTexture.loadFromFile("Resources/MonsterOne.png"))
	{
		std::cout << "Error loading monster texture!" << std::endl;
	}
	mob.sprite.setTexture(monsterTexture);
	mob.sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	mob.sprite.setPosition(160.f, 160.f);

	std::vector < sf::RectangleShape > lines;

	
	while (mainWindow.isOpen())
	{
		localElapsedTime = mainGameClock.restart();
		timeSinceLastUpdate += localElapsedTime;

		while (timeSinceLastUpdate > timePerFrame)
		{
			timeSinceLastUpdate -= timePerFrame;
			sf::Event event;
			while (mainWindow.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					mainWindow.close();
				if (event.type == sf::Event::GainedFocus)
				{
					gameHasFocus = true;
				}
				if (event.type == sf::Event::LostFocus)
				{
					gameHasFocus = false;
				}
				if (event.type = sf::Event::KeyPressed)
				{
					if (event.key.code == sf::Keyboard::Z)
					{
						displayPath = !displayPath;
					}
					if (event.key.code == sf::Keyboard::X)
					{
						AStarPathFind(
							sf::Vector2i(mainTilesList[(unsigned int)player.sprite.getPosition().y / 32][(unsigned int)player.sprite.getPosition().x / 32].posX,
								mainTilesList[(unsigned int)player.sprite.getPosition().y / 32][(unsigned int)player.sprite.getPosition().x / 32].posY),
							sf::Vector2i(mainTilesList[(unsigned int)mob.sprite.getPosition().y / 32][(unsigned int)mob.sprite.getPosition().x / 32].posX,
								mainTilesList[(unsigned int)mob.sprite.getPosition().y / 32][(unsigned int)mob.sprite.getPosition().x / 32].posY));

					}
				}
			}
		}

		player.posX = (int)player.sprite.getPosition().x / 32;
		player.posY = (int)player.sprite.getPosition().y / 32;

		//Line scouts
		lines.clear();
		for (unsigned int i = 0; i < 13; i++)
		{
			sf::RectangleShape line(sf::Vector2f(100, 65));
			line.setPosition(player.sprite.getPosition().x + 16, player.sprite.getPosition().y + 16);
			switch (i)
			{
			case 0:
				line.rotate(0);
				break;
			case 1:
				line.rotate(30);
				break;
			case 2:
				line.rotate(60);
				break;
			case 3:
				line.rotate(90);
				break;
			case 4:
				line.rotate(120);
				break;
			case 5:
				line.rotate(150);
				break;
			case 6:
				line.rotate(180);
				break;
			case 7:
				line.rotate(210);
				break;
			case 8:
				line.rotate(240);
				break;
			case 9:
				line.rotate(270);
				break;
			case 10:
				line.rotate(300);
				break;
			case 11:
				line.rotate(330);
				break;
			case 12:
				line.rotate(360);
				break;
			}
			lines.push_back(line);
		}
		int mouseX = (int)mainWindow.mapPixelToCoords(sf::Mouse::getPosition(mainWindow)).x;
		int mouseY = (int)mainWindow.mapPixelToCoords(sf::Mouse::getPosition(mainWindow)).y;

		//Tile debugging
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			//Move the selector rectangle to where the mouse clicked
			if ((mouseX >= 0) && (mouseY >= 0) && (mouseX / 32 * 32 / 32 < (int)mapSizeX) && (mouseY / 32 * 32 / 32 < (int)mapSizeY))
			{
				std::cout << "Light at " << mouseX / 32 * 32 / 32 << ", " << mouseY / 32 * 32 / 32 << " is " << mainTilesList[mouseY / 32 * 32 / 32][mouseX / 32 * 32 / 32].lightLevel << std::endl;
				std::cout << "Tile: (" << mainTilesList[mouseY / 32 * 32 / 32][mouseX / 32 * 32 / 32].posX << ", " << mainTilesList[mouseY / 32 * 32 / 32][mouseX / 32 * 32 / 32].posY << ")" << std::endl;
			}
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			AStarPathFind(
				sf::Vector2i(mainTilesList[(unsigned int)player.sprite.getPosition().y / 32][(unsigned int)player.sprite.getPosition().x / 32].posX,
					mainTilesList[(unsigned int)player.sprite.getPosition().y / 32][(unsigned int)player.sprite.getPosition().x / 32].posY),
				sf::Vector2i(mainTilesList[(unsigned int)mob.sprite.getPosition().y / 32][(unsigned int)mob.sprite.getPosition().x / 32].posX,
					mainTilesList[(unsigned int)mob.sprite.getPosition().y / 32][(unsigned int)mob.sprite.getPosition().x / 32].posY));
		}

		//Player Movement
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::W)) && (enableScroll == true))
		{
			if (!mainTilesList[player.posY][player.posX].solid &&
				!mainTilesList[player.posY][player.posX + 1].solid)
			{
				player.sprite.move(0, -player.speed);
			}
			else
			{
				player.sprite.setPosition(player.sprite.getPosition().x, player.sprite.getPosition().y + 1);
			}
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)) && (enableScroll == true))
		{
			if (!mainTilesList[player.posY + 1][player.posX].solid &&
				!mainTilesList[player.posY + 1][player.posX + 1].solid)
			{
				player.sprite.move(0, player.speed);
			}
			else
			{
				player.sprite.setPosition(player.sprite.getPosition().x, player.sprite.getPosition().y - 1);
			}
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D)) && (enableScroll == true))
		{
			if (!mainTilesList[player.posY][player.posX].solid &&
				!mainTilesList[player.posY + 1][player.posX + 1].solid)
			{
				player.sprite.move(player.speed, 0);
			}
			else
			{
				player.sprite.setPosition(player.sprite.getPosition().x - 1, player.sprite.getPosition().y);
			}
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A)) && (enableScroll == true))
		{
			if (!mainTilesList[player.posY][player.posX].solid &&
				!mainTilesList[player.posY + 1][player.posX].solid)
			{
				player.sprite.move(-player.speed, 0);
			}
			else
			{
				player.sprite.setPosition(player.sprite.getPosition().x + 1, player.sprite.getPosition().y);
			}
		}
		else
		{
			enableScroll = true;
		}

		scrollView();

		/*---------------------- View Culling -------------------*/
		//View Managing
		viewCenter = view1.getCenter();
		viewSize = view1.getSize();

		//Left X Value to be drawn
		if ((viewCenter.x / 32) - 17 >= 0)
		{
			drawMapX1 = (int)((viewCenter.x / 32) - 17);
		}
		else
		{
			drawMapX1 = 0;
		}
		//Right X Value to be drawn
		if ((viewCenter.x / 32) + 18 < mapSizeX)
		{
			drawMapX2 = (int)(viewCenter.x / 32) + 18;
		}
		else
		{
			drawMapX2 = mapSizeX - 1;
		}
		//Top Y Value to be drawn
		if ((viewCenter.y / 32) - 10 >= 0)
		{
			drawMapY1 = (int)(viewCenter.y / 32) - 10;
		}
		else
		{
			drawMapY1 = 0;
		}
		//Bottom Y Value to be drawn
		if ((viewCenter.y / 32) + 10 < mapSizeY)
		{
			drawMapY2 = (int)(viewCenter.y / 32) + 10;
		}


		/*--------------------- LIGHTING -----------------------*/

		for (int i = drawMapX1; i <= drawMapX2; i++)
		{
			for (int j = drawMapY1; j <= drawMapY2; j++)
			{
				mainTilesList[j][i].lightLevel = 63;

				for (unsigned int k = 0; k < lines.size(); k++)
				{
					//Comment out the following line to eliminate the darkness
					if (lines[k].getGlobalBounds().intersects(mainTilesList[j][i].sprite.getGlobalBounds()))
					{
						mainTilesList[j][i].lightLevel -= 10; //Set to 5
						if (mainTilesList[j][i].lightLevel < 0)
						{
							mainTilesList[j][i].lightLevel = 1;
						}
					}
					if (mainTilesList[j][i].behaviour == 1)
					{
						mainTilesList[j][i].lightLevel = mainTilesList[j][i].tileMeta;
						break;
					}
				}
				
			}
		}



		/*---------------------- RENDERING ----------------------*/

		mainWindow.clear();
		
		mainWindow.setView(view1);

		for (int i = drawMapX1; i <= drawMapX2; i++)
		{
			//X loop
			for (int j = drawMapY1; j <= drawMapY2; j++)
			{
				backgroundTilesList[j][i].sprite.setTexture(tileTexturesList[backgroundTilesList[j][i].texture]);
				backgroundTilesList[j][i].sprite.setPosition((float)i * 32, (float)j * 32);
				mainWindow.draw(backgroundTilesList[j][i].sprite);
			}
		}
		for (int i = drawMapX1; i <= drawMapX2; i++)
		{
			for (int j = drawMapY1; j <= drawMapY2; j++)
			{
				
				mainTilesList[j][i].sprite.setTexture(tileTexturesList[mainTilesList[j][i].texture]);
				mainTilesList[j][i].sprite.setPosition((float)i * 32, (float)j * 32);
				mainWindow.draw(mainTilesList[j][i].sprite);

				if (mainTilesList[j][i].lightLevel != NULL)
				{
					darkness.setColor(sf::Color::Color(0, 0, 0, (sf::Uint8(4 * mainTilesList[j][i].lightLevel))));
				}
				else
				{
					darkness.setColor(sf::Color::Color(0, 0, 0, 255));
				}
				darkness.setPosition(mainTilesList[j][i].sprite.getPosition());
				
				mainWindow.draw(darkness);
			}
		}
		for (int i = drawMapX1; i <= drawMapX2; i++)
		{
			for (int j = drawMapY1; j <= drawMapY2; j++)
			{
				foregroundTilesList[j][i].sprite.setTexture(tileTexturesList[foregroundTilesList[j][i].texture]);
				foregroundTilesList[j][i].sprite.setPosition((float)i * 32, (float)j * 32);
				mainWindow.draw(foregroundTilesList[j][i].sprite);
			}
		}
		///TODO: Draw monsters layer

		
		
		for (unsigned int i = 0; i < lines.size(); i++)
		{
			//mainWindow.draw(lines[i]);
		}

		

		if (displayPath)
		{
			if (astarDebugList.size() > 1)
			{
				for (unsigned int i = 0; i < astarDebugList.size(); i++)
				{
					mainWindow.draw(astarDebugList[i]);
				}

			}
		}
		
		if (mainTilesList[(unsigned int)mob.sprite.getPosition().y / 32][(unsigned int)mob.sprite.getPosition().x / 32].lightLevel < 25)
		{
			mainWindow.draw(mob.sprite);
		}

		mainWindow.draw(player.sprite);


		mainWindow.display();
	}
	return 0;
}
void scrollView()
{
	if (player.sprite.getPosition().x < view1.getCenter().x - (screenSizeX * 0.1))
	{
		if (viewCenter.x < (32 * 17))
		{

		}
		else
		{
			scroll((int)-scrollSpeed, 0);
		}
	}
	if (player.sprite.getPosition().x > view1.getCenter().x + (screenSizeX * 0.1))
	{
		if (viewCenter.x > (mapSizeX * 32) - (32 * 17.1))
		{

		}
		else
		{
			scroll((int)scrollSpeed, 0);
		}
	}
	if (player.sprite.getPosition().y < view1.getCenter().y - (screenSizeY * 0.2))
	{
		if (viewCenter.y < 32 * 9.8)
		{

		}
		else
		{
			scroll(0, (int)-scrollSpeed);
		}
	}
	if (player.sprite.getPosition().y > view1.getCenter().y + (screenSizeY * 0.2))
	{
		if (viewCenter.y > ((mapSizeY * 32)) - (32 * 9.8))
		{

		}
		else
		{
			scroll(0, (int)scrollSpeed);
		}
	}
}