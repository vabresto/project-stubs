#include "stdafx.h"
#include <SFML\Graphics.hpp>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <cctype>
#include <string>
#include <vector>
#include <sstream>
#include <random>

#include "FileDataClasses.h"
#include "FileManager.h"
#include "GlobalVariables.h"

void LoadMap(std::string _fileName)
{
	std::fstream openfile(_fileName);

	//Checking if the file is opened
	if (openfile.is_open())
	{
		openfile >> mapVersion;
		openfile >> numOfTilesets;
		std::cout << "File location: " << _fileName << " was found successfully!\n";

		std::string tileLocation;
		sf::Texture newTexture;
		//Store the location so we can access it
		for (unsigned int i = 0; i < numOfTilesets; i++)
		{
			openfile >> tileLocation;
			tileLocationsList.push_back(tileLocation);


			tileTexturesList.push_back(newTexture);
			if (!tileTexturesList[i].loadFromFile(tileLocation))
			{
				std::cout << "Error loading texture #" << i << " located at " << tileLocation << "\n";
			}


			tileLocation.clear();
		}

		openfile >> mapSizeX;
		openfile >> mapSizeY;

		std::cout << mapSizeX << " " << mapSizeY << " " << std::endl;

		///Layers 1 - 3 are background, play area, and foreground respectively, Layer 4 is for monsters
		for (unsigned int layer = 0; layer < 4; layer++)
		{
			if (layer == 0)
			{
				for (unsigned int i = 0; i <= mapSizeY; i++)
				{
					std::string str, value;
					std::getline(openfile, str);
					std::stringstream stream(str);

					std::vector < tileDeco > tempBackground;

					std::string textureSTR, texXSTR, texYSTR, animFramesSTR, frameTimeSTR;
					unsigned int texture, texX, texY, animFrames, frameTime;

					while (std::getline(stream, value, '.'))
					{
						if (value.length() > 0)
						{
							size_t pos = 0;
							for (unsigned int i = 0; i < 5; i++) //texture, texX, texY, animFrames, frameTime
							{
								pos = value.find(',');
								if (i == 0)
								{
									textureSTR = value.substr(0, pos);
									unsigned int x;											//Working variable
									for (x = 0; x < textureSTR.length(); x++)		//Increment through textureSTR
									{
										if (!isdigit(textureSTR[x]))					//If the xth element of textureSTR is not a digit, break
											break;
									}
									if (x == textureSTR.length())					//If all of textureSTR is a digit, convert it to numbers
									{
										texture = atoi(textureSTR.c_str());
									}
									else											//Otherwise, set to 0 ( error )
									{
										texture = 0;
									}
								}
								else if (i == 1)
								{
									texXSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < texXSTR.length(); x++)
									{
										if (!isdigit(texXSTR[x]))
											break;
									}
									if (x == texXSTR.length())
									{
										texX = atoi(texXSTR.c_str());
									}
									else
									{
										texX = 0;
									}
								}
								else if (i == 2)
								{
									texYSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < texYSTR.length(); x++)
									{
										if (!isdigit(texYSTR[x]))
											break;
									}
									if (x == texYSTR.length())
									{
										texY = atoi(texYSTR.c_str());
									}
									else
									{
										texY = 0;
									}
								}
								else if (i == 3)
								{
									animFramesSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < animFramesSTR.length(); x++)
									{
										if (!isdigit(animFramesSTR[x]))
											break;
									}
									if (x == animFramesSTR.length())
									{
										animFrames = atoi(animFramesSTR.c_str());
									}
									else
									{
										animFrames = 0;
									}
								}
								else if (i == 4)
								{
									frameTimeSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < frameTimeSTR.length(); x++)
									{
										if (!isdigit(frameTimeSTR[x]))
											break;
									}
									if (x == frameTimeSTR.length())
									{
										frameTime = atoi(frameTimeSTR.c_str());
									}
									else
									{
										frameTime = 50;
									}
								}
								value.erase(0, value.find(',') + 1);
							}

							tileDeco newTile;

							newTile.texture = texture;

							newTile.texX = texX;
							newTile.texY = texY;
							newTile.animFrames = animFrames;
							newTile.timeSinceLastUpdate = sf::Time::Zero;
							newTile.frameTime = sf::milliseconds(frameTime);

							newTile.sprite.setTexture(tileTexturesList[newTile.texture]);
							newTile.sprite.setTextureRect(sf::IntRect(newTile.texX * 32, newTile.texY * 32, 32, 32));

							tempBackground.push_back(newTile);
						}
					}
					if (i != 0)	//WHY DOES THIS ONLY HAPPEN HERE !?
					{
						backgroundTilesList.push_back(tempBackground);
						tempBackground.clear();
					}
				}
			}
			if (layer == 1)
			{
				for (unsigned int i = 0; i < mapSizeY; i++)
				{
					std::string str, value;
					std::getline(openfile, str);
					std::stringstream stream(str);

					std::vector < tile > tempMainMap;

					std::string textureSTR, texXSTR, texYSTR, solidSTR, healthSTR, animFramesSTR, behaviourSTR, frameTimeSTR, tileMetaSTR, tileMeta2STR;
					unsigned int texture, texX, texY, solid, health, animFrames, behaviour, frameTime, tileMeta, tileMeta2;

					while (std::getline(stream, value, '.'))
					{
						if (value.length() > 0)
						{
							size_t pos = 0;
							for (unsigned int i = 0; i < 10; i++) //texture, texX, texY, solid, health, animFrames, behaviour, timePerFrame, tileMeta, tileMeta2
							{
								pos = value.find(',');
								if (i == 0)
								{
									textureSTR = value.substr(0, pos);
									unsigned int x;											//Working variable
									for (x = 0; x < textureSTR.length(); x++)		//Increment through textureSTR
									{
										if (!isdigit(textureSTR[x]))					//If the xth element of textureSTR is not a digit, break
											break;
									}
									if (x == textureSTR.length())					//If all of textureSTR is a digit, convert it to numbers
									{
										texture = atoi(textureSTR.c_str());
									}
									else											//Otherwise, set to 0 ( error )
									{
										texture = 0;
									}
								}
								else if (i == 1)
								{
									texXSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < texXSTR.length(); x++)
									{
										if (!isdigit(texXSTR[x]))
											break;
									}
									if (x == texXSTR.length())
									{
										texX = atoi(texXSTR.c_str());
									}
									else
									{
										texX = 0;
									}
								}
								else if (i == 2)
								{
									texYSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < texYSTR.length(); x++)
									{
										if (!isdigit(texYSTR[x]))
											break;
									}
									if (x == texYSTR.length())
									{
										texY = atoi(texYSTR.c_str());
									}
									else
									{
										texY = 0;
									}
								}
								else if (i == 3)
								{
									solidSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < solidSTR.length(); x++)
									{
										if (!isdigit(solidSTR[x]))
											break;
									}
									if (x == solidSTR.length())
									{
										solid = atoi(solidSTR.c_str());
									}
									else
									{
										solid = 0;
									}
								}
								else if (i == 4)
								{
									healthSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < healthSTR.length(); x++)
									{
										if (!isdigit(healthSTR[x]))
											break;
									}
									if (x == healthSTR.length())
									{
										health = atoi(healthSTR.c_str());
									}
									else
									{
										health = 0;
									}
								}
								else if (i == 5)
								{
									animFramesSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < animFramesSTR.length(); x++)
									{
										if (!isdigit(animFramesSTR[x]))
											break;
									}
									if (x == animFramesSTR.length())
									{
										animFrames = atoi(animFramesSTR.c_str());
									}
									else
									{
										animFrames = 0;
									}
								}
								else if (i == 6)
								{
									behaviourSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < behaviourSTR.length(); x++)
									{
										if (!isdigit(behaviourSTR[x]))
											break;
									}
									if (x == behaviourSTR.length())
									{
										behaviour = atoi(behaviourSTR.c_str());
									}
									else
									{
										behaviour = 0;
									}
								}
								else if (i == 7)
								{
									frameTimeSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < frameTimeSTR.length(); x++)
									{
										if (!isdigit(frameTimeSTR[x]))
											break;
									}
									if (x == frameTimeSTR.length())
									{
										frameTime = atoi(frameTimeSTR.c_str());
									}
									else
									{
										frameTime = 0;
									}
								}
								else if (i == 8)
								{
									tileMetaSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < tileMetaSTR.length(); x++)
									{
										if (!isdigit(tileMetaSTR[x]))
											break;
									}
									if (x == tileMetaSTR.length())
									{
										tileMeta = atoi(tileMetaSTR.c_str());
									}
									else
									{
										tileMeta = 0;
									}
								}
								else if (i == 9)
								{
									tileMeta2STR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < tileMeta2STR.length(); x++)
									{
										if (!isdigit(tileMeta2STR[x]))
											break;
									}
									if (x == tileMeta2STR.length())
									{
										tileMeta2 = atoi(tileMeta2STR.c_str());
									}
									else
									{
										tileMeta2 = 0;
									}
								}
								value.erase(0, value.find(',') + 1);
							}

							tile newTile;

							newTile.texture = texture;

							newTile.texX = texX;
							newTile.texY = texY;
							newTile.solid = (bool)solid;
							newTile.health = (float)health;
							newTile.animFrames = animFrames;
							newTile.behaviour = behaviour;
							newTile.frameTime = sf::milliseconds(frameTime);
							newTile.tileMeta = (float)tileMeta;
							newTile.tileMeta2 = (float)tileMeta2;

							newTile.posX = tempMainMap.size();
							newTile.posY = i;

							newTile.lightLevel = 63;

							newTile.sprite.setTexture(tileTexturesList[newTile.texture]);
							newTile.sprite.setTextureRect(sf::IntRect(newTile.texX * 32, newTile.texY * 32, 32, 32));

							tempMainMap.push_back(newTile);
						}
					}
					mainTilesList.push_back(tempMainMap);
					tempMainMap.clear();
				}
			}
			if (layer == 2)
			{
				//file << "0,0,0,0.";	//Texture, tileX, tileY, animFrames
				for (unsigned int i = 0; i < mapSizeY; i++)
				{
					std::string str, value;
					std::getline(openfile, str);
					std::stringstream stream(str);

					std::vector < tileDeco > tempForeground;

					std::string textureSTR, texXSTR, texYSTR, animFramesSTR, frameTimeSTR;
					unsigned int texture, texX, texY, animFrames, frameTime;

					while (std::getline(stream, value, '.'))
					{
						if (value.length() > 0)
						{
							size_t pos = 0;
							for (unsigned int i = 0; i < 5; i++) //texture, texX, texY, animFrames, frameTime
							{
								pos = value.find(',');
								if (i == 0)
								{
									textureSTR = value.substr(0, pos);
									unsigned int x;											//Working variable
									for (x = 0; x < textureSTR.length(); x++)		//Increment through textureSTR
									{
										if (!isdigit(textureSTR[x]))					//If the xth element of textureSTR is not a digit, break
											break;
									}
									if (x == textureSTR.length())					//If all of textureSTR is a digit, convert it to numbers
									{
										texture = atoi(textureSTR.c_str());
									}
									else											//Otherwise, set to 0 ( error )
									{
										texture = 0;
									}
								}
								else if (i == 1)
								{
									texXSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < texXSTR.length(); x++)
									{
										if (!isdigit(texXSTR[x]))
											break;
									}
									if (x == texXSTR.length())
									{
										texX = atoi(texXSTR.c_str());
									}
									else
									{
										texX = 0;
									}
								}
								else if (i == 2)
								{
									texYSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < texYSTR.length(); x++)
									{
										if (!isdigit(texYSTR[x]))
											break;
									}
									if (x == texYSTR.length())
									{
										texY = atoi(texYSTR.c_str());
									}
									else
									{
										texY = 0;
									}
								}
								else if (i == 3)
								{
									animFramesSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < animFramesSTR.length(); x++)
									{
										if (!isdigit(animFramesSTR[x]))
											break;
									}
									if (x == animFramesSTR.length())
									{
										animFrames = atoi(animFramesSTR.c_str());
									}
									else
									{
										animFrames = 0;
									}
								}
								else if (i == 4)
								{
									frameTimeSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < frameTimeSTR.length(); x++)
									{
										if (!isdigit(frameTimeSTR[x]))
											break;
									}
									if (x == frameTimeSTR.length())
									{
										frameTime = atoi(frameTimeSTR.c_str());
									}
									else
									{
										frameTime = 0;
									}
								}
								value.erase(0, value.find(',') + 1);
							}

							tileDeco newTile;

							newTile.texture = texture;

							newTile.texX = texX;
							newTile.texY = texY;
							newTile.animFrames = animFrames;
							newTile.timeSinceLastUpdate = sf::Time::Zero;
							newTile.frameTime = sf::milliseconds(frameTime);

							newTile.sprite.setTexture(tileTexturesList[newTile.texture]);
							newTile.sprite.setTextureRect(sf::IntRect(newTile.texX * 32, newTile.texY * 32, 32, 32));

							tempForeground.push_back(newTile);
						}
					}
					foregroundTilesList.push_back(tempForeground);
					tempForeground.clear();
				}
			}
			if (layer == 3)
			{
				for (unsigned int i = 0; i < mapSizeY; i++)
				{
					std::string str, value;
					std::getline(openfile, str);
					std::stringstream stream(str);

					std::string textureSTR, healthSTR, speedSTR, behaviourSTR, typeSTR, animFramesSTR, currentFrameSTR, frameTimeSTR, directionSTR, spawnTileXSTR, spawnTileYSTR;
					unsigned int texture, health, speed, behaviour, type, animFrames, currentFrame, frameTime, direction, spawnTileX, spawnTileY;

					while (std::getline(stream, value, '.'))
					{
						if (value.length() > 0)
						{
							size_t pos = 0;
							monster newMonster;	//texture, health, speed, behaviour, type, animFrames, currentFrame, frameTime, direction, spawnTileX, spawnTileY

							for (unsigned int i = 0; i < 11; i++)
							{
								pos = value.find(',');
								if (i == 0)
								{
									textureSTR = value.substr(0, pos);
									unsigned int x;									//Working variable
									for (x = 0; x < textureSTR.length(); x++)		//Increment through textureSTR
									{
										if (!isdigit(textureSTR[x]))					//If the xth element of textureSTR is not a digit, break
											break;
									}
									if (x == textureSTR.length())					//If all of textureSTR is a digit, convert it to numbers
									{
										texture = atoi(textureSTR.c_str());
									}
									else											//Otherwise, set to 0 ( error )
									{
										texture = 0;
									}
								}
								else if (i == 1)
								{
									healthSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < healthSTR.length(); x++)
									{
										if (!isdigit(healthSTR[x]))
											break;
									}
									if (x == healthSTR.length())
									{
										health = atoi(healthSTR.c_str());
									}
									else
									{
										health = 0;
									}
								}
								else if (i == 2)
								{
									speedSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < speedSTR.length(); x++)
									{
										if (!isdigit(speedSTR[x]))
											break;
									}
									if (x == speedSTR.length())
									{
										speed = atoi(speedSTR.c_str());
									}
									else
									{
										speed = 0;
									}
								}
								else if (i == 3)
								{
									behaviourSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < behaviourSTR.length(); x++)
									{
										if (!isdigit(behaviourSTR[x]))
											break;
									}
									if (x == behaviourSTR.length())
									{
										behaviour = atoi(behaviourSTR.c_str());
									}
									else
									{
										behaviour = 0;
									}
								}
								else if (i == 4)
								{
									typeSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < typeSTR.length(); x++)
									{
										if (!isdigit(typeSTR[x]))
											break;
									}
									if (x == typeSTR.length())
									{
										type = atoi(typeSTR.c_str());
									}
									else
									{
										type = 0;
									}
								}
								else if (i == 5)
								{
									animFramesSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < animFramesSTR.length(); x++)
									{
										if (!isdigit(animFramesSTR[x]))
											break;
									}
									if (x == animFramesSTR.length())
									{
										animFrames = atoi(animFramesSTR.c_str());
									}
									else
									{
										animFrames = 0;
									}
								}
								else if (i == 6)
								{
									currentFrameSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < currentFrameSTR.length(); x++)
									{
										if (!isdigit(currentFrameSTR[x]))
											break;
									}
									if (x == currentFrameSTR.length())
									{
										currentFrame = atoi(currentFrameSTR.c_str());
									}
									else
									{
										currentFrame = 0;
									}
								}
								else if (i == 7)
								{
									frameTimeSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < frameTimeSTR.length(); x++)
									{
										if (!isdigit(frameTimeSTR[x]))
											break;
									}
									if (x == frameTimeSTR.length())
									{
										frameTime = atoi(frameTimeSTR.c_str());
									}
									else
									{
										frameTime = 50;
									}
								}
								else if (i == 8)
								{
									directionSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < directionSTR.length(); x++)
									{
										if (!isdigit(directionSTR[x]))
											break;
									}
									if (x == directionSTR.length())
									{
										direction = atoi(directionSTR.c_str());
									}
									else
									{
										direction = 0;
									}
								}
								else if (i == 9)
								{
									spawnTileXSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < spawnTileXSTR.length(); x++)
									{
										if (!isdigit(spawnTileXSTR[x]))
											break;
									}
									if (x == spawnTileXSTR.length())
									{
										spawnTileX = atoi(spawnTileXSTR.c_str());
									}
									else
									{
										spawnTileX = 0;
									}
								}
								else if (i == 10)
								{
									spawnTileYSTR = value.substr(0, pos);
									unsigned int x;
									for (x = 0; x < spawnTileYSTR.length(); x++)
									{
										if (!isdigit(spawnTileYSTR[x]))
											break;
									}
									if (x == spawnTileYSTR.length())
									{
										spawnTileY = atoi(spawnTileYSTR.c_str());
									}
									else
									{
										spawnTileY = 0;
									}
								}
								value.erase(0, value.find(',') + 1);
							}

							newMonster.texture = texture;
							newMonster.health = (float)health;
							newMonster.speed = (float)speed;
							newMonster.behaviour = behaviour;
							newMonster.type = type;
							newMonster.animFrames = animFrames;
							newMonster.currentFrame = currentFrame;
							newMonster.frameTime = sf::milliseconds(frameTime);
							newMonster.direction = direction;
							newMonster.spawnTileX = spawnTileX;
							newMonster.spawnTileY = spawnTileY;

							newMonster.sprite.setTexture(tileTexturesList[newMonster.texture]);
							newMonster.sprite.setTextureRect(sf::IntRect(32, 32, 32, 32));

							monstersList.push_back(newMonster);
						}
					}
				}
			}
		}
	}
	else
	{
		std::cout << "File could not be opened! \n";
	}
}

void SaveMap(std::string _fileName)
{
	std::fstream openfile;
	/*****************/
	//Save map to file
	/****************/
	std::cout << "Saving...\n";
	openfile.close();
	openfile.open(_fileName);

	openfile << mapVersion << std::endl;

	openfile << numOfTilesets << std::endl;

	for (unsigned int i = 0; i < numOfTilesets; i++)
	{
		openfile << tileLocationsList[i] << std::endl;
	}

	openfile << mapSizeX << std::endl;
	openfile << mapSizeY << std::endl;

	//Background
	for (unsigned int i = 0; i < backgroundTilesList.size(); i++)
	{
		for (unsigned int j = 0; j < backgroundTilesList[i].size(); j++)
		{
			openfile << backgroundTilesList[i][j].texture << "," << backgroundTilesList[i][j].texX << "," << backgroundTilesList[i][j].texY << "," << backgroundTilesList[i][j].animFrames << "," << backgroundTilesList[i][j].frameTime.asMilliseconds() << ".";
		}
		openfile << "\n";
	}

	//Main ground
	for (unsigned int i = 0; i < mainTilesList.size(); i++)
	{
		for (unsigned int j = 0; j < mainTilesList[i].size(); j++)
		{
			openfile << mainTilesList[i][j].texture << "," << mainTilesList[i][j].texX << "," << mainTilesList[i][j].texY << "," << mainTilesList[i][j].solid << "," << mainTilesList[i][j].health << "," << mainTilesList[i][j].animFrames << "," << mainTilesList[i][j].behaviour << "," << mainTilesList[i][j].frameTime.asMilliseconds() << "," << mainTilesList[i][j].tileMeta << "," << mainTilesList[i][j].tileMeta2 << ".";
		}
		openfile << "\n";
	}

	//Foreground
	for (unsigned int i = 0; i < foregroundTilesList.size(); i++)
	{
		for (unsigned int j = 0; j < foregroundTilesList[i].size(); j++)
		{
			openfile << foregroundTilesList[i][j].texture << "," << foregroundTilesList[i][j].texX << "," << foregroundTilesList[i][j].texY << "," << foregroundTilesList[i][j].animFrames << "," << foregroundTilesList[i][j].frameTime.asMilliseconds() << ".";
		}
		openfile << "\n";
	}

	//Monsters
	for (unsigned int i = 0; i < monstersList.size(); i++)
	{
		openfile << monstersList[i].texture << "," << monstersList[i].health << "," << monstersList[i].speed << "," << monstersList[i].behaviour << "," << monstersList[i].type << "," << monstersList[i].animFrames << "," << monstersList[i].currentFrame << "," << monstersList[i].frameTime.asMilliseconds() << "," << monstersList[i].direction << "," << monstersList[i].spawnTileX << "," << monstersList[i].spawnTileY << ".";
	}
	openfile << "\n";

	//Inform the user that the save has completed
	std::cout << "Saving complete!\n";
}