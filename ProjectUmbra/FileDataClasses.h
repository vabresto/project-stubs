#pragma once
//#include <SFML\Graphics.hpp>
#include "CharacterManager.h"
///Things with 3 backslashes get saved to file; the rest do not

struct tile
{
	float lightLevel;				//Light level of tile - HAS GAMEPLAY IMPACT
	unsigned int texture;			///Texture number; corresponds to index of texturesList
	unsigned int texX;				///Texture's X coordinate
	unsigned int texY;				///Texture's Y coordinate
	bool solid;						///Players can walk through it
	float health;					///Tool to make destructible tiles
	unsigned int animFrames;		///Animation frames tile has available
	unsigned int currentAnimFrame;	//Animation frame tile starts with - same as texY
	int behaviour;					///Script to be run if the player steps on this tile
	sf::Time frameTime;				///
	sf::Time timeSinceLastUpdate;
	float tileMeta;					///Used to store whatever other info about the tile is needed ie.fuel left
	float tileMeta2;				///Extra slot to store info ie. brightness

	unsigned int posX;				//posX and posY correspond to the tile's location in mainTilesList
	unsigned int posY;				//Do not need to be saved, are generated at load; used for path finding

	sf::Sprite sprite;
};

struct tileDeco						///Layers 1 and 3
{
	float lightLevel;				//Light level of decoration - NO GAMEPLAY IMPACT
	unsigned int texture;			///
	unsigned int texX;				///
	unsigned int texY;				///
	unsigned int animFrames;		///
	unsigned int currentAnimFrame;	//Same as texY when file loads
	sf::Time frameTime;				///
	sf::Time timeSinceLastUpdate;

	sf::Sprite sprite;
};

struct monster
{
	int texture;					///Every monster has its own texture!
	float health;					///Monster health - used as a timer for how long they can be in the light
	float speed;					///Movement speed of monster; multiplier to base speed
	float visible;					//Visibility level of monster (in percent 0-1)
	int behaviour;					///The AI the monster will use; variable used as switch
	int type;						///Type of monster
	unsigned int animFrames;		///
	unsigned int currentFrame;	///
	sf::Time frameTime;			///
	int direction;				///The direction the monster is facing
	int spawnTileX;					///
	int spawnTileY;					///

	sf::Sprite sprite;

	void update()
	{

	}
};
