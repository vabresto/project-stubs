#pragma once
#include "FileDataClasses.h"

extern int screenSizeX;
extern int screenSizeY;

extern int localPlayerNumber;
extern int iAmPlayerNumber;

extern sf::RenderWindow mainWindow;
extern sf::View view1;

extern sf::Time localElapsedTime;
extern sf::Time timeSinceLastUpdate;
extern const sf::Time timePerFrame;

extern sf::Clock mainGameClock;

extern bool gameHasFocus;

extern std::vector<sf::Texture>texturesList();

/*--------- MAP DATA --------------*/
extern unsigned int mapVersion;
extern unsigned int numOfTilesets;

extern unsigned int mapSizeX, mapSizeY;

extern std::vector < std::string > tileLocationsList;
extern std::vector < sf::Texture > tileTexturesList;

extern std::vector < std::vector < tileDeco > > backgroundTilesList;
extern std::vector < std::vector < tile > > mainTilesList;
extern std::vector < std::vector < tileDeco > > foregroundTilesList;
extern std::vector < monster > monstersList;

/*--------- MAP DRAWING ----------------*/
extern sf::Vector2f viewCenter;
extern sf::Vector2f viewSize;
extern int drawMapX1, drawMapX2, drawMapY1, drawMapY2;
extern float scrollSpeed;
extern bool enableScroll;

/*------------ GAME DATA ------------------*/
extern std::vector < sf::Texture > characterTexturesList;
extern std::vector < sf::Sprite > astarDebugList;
extern sf::Texture blackTexture;
extern sf::Texture markerTexture;
//extern std::vector < player > playersList;