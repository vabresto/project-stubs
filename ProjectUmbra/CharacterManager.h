#pragma once
#include <SFML\Graphics.hpp>

struct character
{
public:
	int texture;				//Index reference to characterTexturesList
	int texX;
	int texY;

	float health;
	float speed;

	int posX, posY;

	int animFrames;
	int currentFrame;
	sf::Time frameTime;

	int direction;				///1 - North, 2 - East, 3 - South, 4 - West

	sf::Sprite sprite;
};

/*class player : character
{
public:
	void update()				//Executes once per tick
	{

	}

	void MoveLeft()
	{
		direction = 4;
		texY = 2;
		sprite.move(-speed, 0);
	}

	void MoveRight()
	{
		direction = 2;
		texY = 3;
		sprite.move(speed, 0);
	}

	void MoveUp()
	{
		direction = 1;
		texY = 4;
		sprite.move(0, speed);
	}

	void MoveDown()
	{
		direction = 3;
		texY = 1;
		sprite.move(0, -speed);
	}


};*/
