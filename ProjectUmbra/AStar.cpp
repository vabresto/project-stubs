#include "stdafx.h"
#include <vector>
#include <math.h>

#include <iostream>
#include <algorithm>
#include <memory>

#include "AStar.h"
#include "FileDataClasses.h"
#include "GlobalVariables.h"

float DistanceToNode(sf::Vector2i _start, sf::Vector2i _target);
std::vector < node* > getNeighbours(node* _node);
//void RetracePath(node _start, node _end);
void RetracePath(node _start, node _end, std::vector<node> checkedList);
std::vector <node*> RetracePathPointer(node* _start, node* _end);

void AStarPathFind(sf::Vector2i _start, sf::Vector2i _target)
{

	///Using node pointers results in >10000 pointers created, almost all filled with garbage
	///Using node results in parent being a reference, and thus, useless for tracing back path

	///Mostly works, code is very expensive, however
	std::vector < node* > openList;
	std::vector < node* > closedList;

	bool pathFound = false;

	sf::Clock aStar;
	aStar.restart();

	node* startNode;
	startNode = new node;
	startNode->posX = _start.x;
	startNode->posY = _start.y;
	startNode->gCost = 0;
	startNode->hCost = 0;
	startNode->fCost = 0;

	openList.push_back(startNode);

	while (openList.size() > 0)
	{
		node* current;
		current = new node;
		current = openList[0];
		int search = 0;

		for (unsigned int i = 0; i < openList.size(); i++)
		{
			if (openList[i]->fCost < current->fCost ||
				(openList[i]->fCost == current->fCost && openList[i]->hCost < current->hCost))
			{
				current = openList[i];
				search = i;
			}
		}

		closedList.push_back(current);
		openList.erase(openList.begin() + search);
		
		if (current->posX == _target.x &&
			current->posY == _target.y)
		{
			std::cout << "Done in " << aStar.getElapsedTime().asMilliseconds() << " milliseconds!" << std::endl;

			astarDebugList.clear();
			RetracePathPointer(startNode, current);
			while (!closedList.empty()) delete closedList.back(), closedList.pop_back();
			return;
		}

		std::vector < node* > neighbour = getNeighbours(current);

		for (unsigned int i = 0; i < neighbour.size(); i++)
		{
			if (mainTilesList[neighbour[i]->posY][neighbour[i]->posX].solid)
			{
				continue;
			}
			bool inOpen = false;
			for (unsigned int j = 0; j < closedList.size(); j++)					//This used to be open list, but produced duplicates - this seems to work too ... ?
			{
				if (closedList[j]->posX == neighbour[i]->posX &&
					closedList[j]->posY == neighbour[i]->posY)
				{
					inOpen = true;
					break;
				}
			}

			int newMoveCost = current->gCost + DistanceToNode(
				sf::Vector2i(current->posX, current->posY),
				sf::Vector2i(neighbour[i]->posX, neighbour[i]->posY));

			if (newMoveCost < neighbour[i]->gCost || !inOpen)
			{
				neighbour[i]->parent = NULL;
				neighbour[i]->gCost = (float)newMoveCost;
				neighbour[i]->hCost = DistanceToNode(
					sf::Vector2i(neighbour[i]->posX, neighbour[i]->posY),
					_target);
				neighbour[i]->fCost = neighbour[i]->gCost + neighbour[i]->hCost;
				neighbour[i]->parent = current;

				if (neighbour[i]->posX > current->posX)		//Must be to the right
				{
					if (neighbour[i]->posY > current->posY)	//To the right, and up
					{
						neighbour[i]->parentDirection = 2;
					}
					else if (neighbour[i]->posY == current->posY)	//To the right, same height
					{
						neighbour[i]->parentDirection = 3;
					}
					else										//To the right, and down
					{
						neighbour[i]->parentDirection = 4;
					}
				}
				else if (neighbour[i]->posX == current->posX)		//Above or below
				{
					if (neighbour[i]->posY > current->posY)
					{
						neighbour[i]->parentDirection = 1;
					}
					else
					{
						neighbour[i]->parentDirection = 5;
					}
				}
				else											//Must be to the left
				{
					if (neighbour[i]->posY > current->posY)		//To the left and up
					{
						neighbour[i]->parentDirection = 8;
					}
					else if (neighbour[i]->posY == current->posY)	//Just to the left
					{
						neighbour[i]->parentDirection = 7;
					}
					else										//Down and to the left
					{
						neighbour[i]->parentDirection = 6;
					}
				}

				if (!inOpen)
				{
					openList.push_back(neighbour[i]);
				}
			}
		}
	}
}

float DistanceToNode(sf::Vector2i _start, sf::Vector2i _target)
{
	int dstX = abs(_start.x - _target.x);
	int dstY = abs(_start.y - _target.y);

	if (dstX > dstY)
		return (float)(14 * dstY + 10 * (dstX - dstY));
	else
		return (float)(14 * dstX + 10 * (dstY - dstX));
}

std::vector < node* > getNeighbours(node* _node)
{
	std::vector < node* > neighboursList;

	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			if (x == 0 && y == 0)
				continue;

			int checkX = _node->posX + x;
			int checkY = _node->posY + y;

			//if (checkX >= 0 && checkX <= mainTilesList[0].size )
			///TODO: Verify that target is within map
			node* newNode;
			newNode = new node;
			newNode->posX = mainTilesList[checkY][checkX].posX;
			newNode->posY = mainTilesList[checkY][checkX].posY;

			neighboursList.push_back(newNode);
		}
	}

	return neighboursList;
}

std::vector <node*> RetracePathPointer(node* _start, node* _end)
{
	std::vector < node* > path;
	node* current;
	current = new node;
	current = _end;

	while (current->posX != _start->posX &&
		current->posY != _start->posY)
	{
		sf::Sprite newSprite;
		newSprite.setTexture(markerTexture);
		newSprite.setPosition((float)current->posX * 32, (float)current->posY * 32);
		astarDebugList.push_back(newSprite);
		//std::cout << "(" << current->posX << ", " << current->posY << ")" << std::endl;
		path.push_back(current);
		current = current->parent;
	}
	return path;
}

/*void RetracePath(node _start, node _end, std::vector<node> checkedList)
{
	std::vector <node> path;
	node current;
	current = _end;

	while (current.posX != _start.posX &&
		current.posY != _start.posY)
	{
		std::cout << current.parentDirection << std::endl;
		switch (current.parentDirection)
		{
		case 1:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX && checkedList[i].posY == current.posY + 1)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		case 2:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX + 1 && checkedList[i].posY == current.posY + 1)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		case 3:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX + 1 && checkedList[i].posY == current.posY)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		case 4:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX + 1 && checkedList[i].posY == current.posY - 1)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		case 5:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX && checkedList[i].posY == current.posY - 1)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		case 6:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX - 1 && checkedList[i].posY == current.posY - 1)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		case 7:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX - 1 && checkedList[i].posY == current.posY)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		case 8:
		{
			for (int unsigned i = 0; i < checkedList.size(); i++)
			{
				if (checkedList[i].posX == current.posX - 1 && checkedList[i].posY == current.posY + 1)
				{
					current = checkedList[i];
					break;
				}
			}
		}
		default:
			break;
		}
		std::cout << "Tile: (" << current.posX << ", " << current.posY << ")" << std::endl;
	}
}*/