#pragma once
#include "FileDataClasses.h"

extern void AStarPathFind(sf::Vector2i _start, sf::Vector2i _target);

struct node
{
	float hCost, fCost, gCost;
	unsigned int posX, posY;
	node* parent;
	
	/*
	1: Up
	2: Up - Right
	3: Right
	4: Down - Right
	5: Down
	6: Down - Left
	7: Left
	8: Up - Left
	*/
	int parentDirection;	

};

